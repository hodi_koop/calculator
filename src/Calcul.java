/**
 * Класс Calc содержит методы: сложения, вычитания, умножения, целочисленного деления, вещественного деления,
 * выделение остатка, возведение в степень, извлечение квадратного корня.
 * @author Скворцова Татьяна
 */
public class Calcul {

    /**
     * Возвращает сумму двух аргументов
     *
     * @param operand_1 - превый аргумент
     * @param operand_2 - второй аргумент
     * @return - operand_1 + operand_2;
     */
    public static double add(double operand_1, double operand_2) {
        return operand_1 + operand_2;
    }

    /**
     * Возвращает разность двух аргументов
     *
     * @param operand_1 - первый аргумент
     * @param operand_2 - второй аргумент
     * @return - operand_1 - operand_2;
     */

    public static double sub(double operand_1, double operand_2) {
        return operand_1 - operand_2;
    }

    /**
     * Возвращает произведение двух аргументов
     *
     * @param operand_1 - первый аргумент
     * @param operand_2 - второй аргумент
     * @return - operand_1 * operand_2;
     */
    public static double mult(double operand_1, double operand_2) {
        return operand_1 * operand_2;
    }

    /**
     * Возвращает частное двух  аргументов целочисленного типа
     *
     * @param operand_1 - первый аргумент
     * @param operand_2 - второй агрумент
     * @return - operand_1 / operand_2;
     */
    public static int div(int operand_1, int operand_2) {
        if (operand_2 == 0) {
            throw new ArithmeticException("На ноль делить нельзя");
        }
        return operand_1 / operand_2;
    }

    /**
     * Возвращает частное двух аргументов вещественного типа
     *
     * @param operand_1 - первый аргумент
     * @param operand_2 - второй агрумент
     * @return - operand_1 / operand_2;
     */

    public static double dive(double operand_1, double operand_2) {
        if (operand_2 == 0) {
            throw new ArithmeticException("На ноль делить нельзя");
        }
        return operand_1 / operand_2;
    }

    /**
     * Возвращает остаток от деления двух аргуметов
     *
     * @param operand_1 - первый аргумент
     * @param operand_2 - второй агрумент
     * @return - operand_1 % operand_2;
     */
    public static double balance(double operand_1, double operand_2) {
        return operand_1 % operand_2;
    }

    /**
     * Возвращает число в степени
     *
     * @param operand_1 - первый аргумент, аргумент возводимый в степень
     * @param operand_2 - второй аргумент, степень
     * @return - result;
     */
    public static double power(double operand_1, double operand_2) {
        int result = 1;
        for (double i = operand_2; i > 0; i--) {
            result *= operand_1;
        }
        return result;
    }

    /**
     * Возвращает извлеченный квадратный корень
     *
     * @param operand_1 - аргумент, из которого квадратный корень извлекается
     * @return - x;
     */
    public static double sqrt(double operand_1) {
        double x = 1;
        double eps = 0.01;
        for (; ; ) {
            double nx = (x + operand_1 / x) / 2;
            if (Math.abs(x - nx) < eps) break;
            x = nx;
        }
        return x;
    }
}