import java.io.*;
import java.nio.Buffer;
import java.util.Scanner;
import static java.lang.Double.parseDouble;
/*
class Main - Демонстрационный класс
 */
public class Main {

    public static void main(String[] args) throws IOException {

        Scanner in = new Scanner(System.in);

        System.out.println("Если ввод с клавиатуры введите 1, если с файла 2");
        int select_input = in.nextInt();
        if (select_input == 1) {

            System.out.println("Введите первый операнд:");
            double operand_1 = in.nextDouble();
            System.out.println("Введите знак операции:");
            String op = in.next();
            double operand_2 = 0;

            if (op != "#") {
                System.out.println("Введите второй операнд:");
                operand_2 = in.nextDouble();
            }
            in.close();

            System.out.println(operationSelection(operand_1, op, operand_2));
        }
        else {
            BufferedReader inputData = new BufferedReader(new FileReader("C:\\Users\\user\\Documents\\WorkSpace\\calculator\\out\\production\\Calculator\\input.txt"));
            BufferedWriter outData = new BufferedWriter(new FileWriter("C:\\Users\\user\\Documents\\WorkSpace\\calculator\\out\\production\\Calculator\\out.txt"));

            String dataStr=inputData.readLine();
            while (dataStr != null ) {
                String strMass[] = dataStr.split(" ");

                double operand_1 = Double.parseDouble(strMass[0]);
                System.out.println(operand_1);
                String op = strMass[1];
                System.out.println(op);
                double operand_2 = Double.parseDouble(strMass[2]);
                System.out.println(operand_2);
                double result=operationSelection(operand_1,op,operand_2);
                outData.write(result + "\n");
                dataStr = inputData.readLine();
        }
            inputData.close();
            outData.close();
        }
        }
/*
 class operationSelection - класс выбора оператора и операции
 */
    public static double operationSelection(double operand_1, String op, double operand_2) {
        double res = 0;
        switch (op) {
            case "+":
                res = Calcul.add(operand_1, operand_2);
                break;
            case "-":
                res = Calcul.sub(operand_1, operand_2);
                break;
            case "*":
                res = Calcul.mult(operand_1, operand_2);
                break;
            case "/":
                res = Calcul.dive(operand_1, operand_2);
                break;
            case "%":
                res = Calcul.balance(operand_1, operand_2);
                break;
            case "^":
                res = Calcul.power(operand_1, operand_2);
                break;
            case "#":
                res = Calcul.sqrt(operand_1);
                break;
        }
        return res;
    }
}